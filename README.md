# JetsonNanoTF2

- Wheel file to install tensorflow2 in jetson nano. To install tensorflow2 say
 
```
sudo pip3 uninstall -y tensorboard tensorflow
sudo rm /usr/local/lib/libproto*
sudo rm /usr/local/bin/protoc
sudo pip3 uninstall -y protobuf
sudo rm /usr/local/bin/bazel
sudo pip3 install protobuf==3.8.0
sudo apt-get install -y libhdf5-serial-dev hdf5-tools
sudo pip3 install -U pip six numpy wheel setuptools mock h5py
sudo pip3 install -U keras_applications
sudo pip3 install -U keras_preprocessing
export LD_LIBRARY_PATH=/usr/local/cuda/extras/CUPTI/lib64:$LD_LIBRARY_PATH

sudo pip3 install tensorflow-2.0.0-*.whl
```

- In case of problems, see
https://jkjung-avt.github.io/build-tensorflow-2.0.0/
because all the lines above are copy pasted from there

- Probably I had sdcard image nv-jetson-nano-sd-card-image-r32.2.3

- It took me about 80h to compile tensorflow2 in jetson nano from source. With this wheel file and 
working internet connection you can do the installation of tensorflow2 in jetson nano in less thatn 30 min.

- All this is based on (and many thank's!!!)
https://jkjung-avt.github.io/build-tensorflow-2.0.0/

- to my experience nvidia support has zero value